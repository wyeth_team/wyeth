(function($) {
	"use strict";
	function parallaxify() {
			var screen_width = $(document).width();
			if ( screen_width > 767 ) {
				$.parallaxify(); 
			} else {
				$.parallaxify('destroy');
			}
		}

	$(document).ready(function(){
		parallaxify();
	});

	$(window).resize(function(){
		parallaxify();
	});

	// $('.profile-carousel').on('slide.bs.carousel', function(){
	// 	FB.XFBML.parse();
	// }); 
})(jQuery);
